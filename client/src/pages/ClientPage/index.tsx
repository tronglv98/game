import React, { useEffect, useState } from 'react';
import {Container } from 'react-bootstrap';
import ChatBox from '../../components/ChatBox';
import UsersBox from '../../components/UsersBox';
import { socket } from '../../utils';

function ClientPage() {
  const [users, setUsers] = useState<any[]>([]);
  const [userColors, setUserColors] = useState<any>({});
  const [receiver, setReceiver] = useState<any | undefined>(undefined)
  const [listMessages, setListMessages] = useState<any[]>([]);

  // const handeClick: any = async (color: string) => {
  //   let data: Params = { color: color };
  //   socket.emit('receive-click', data);
  // }
  useEffect(() => {
    socket.on('user-online', async(data) => {
      const colors = userColors;
      colors[data.id] = '#' + Math.random().toString(16).substr(-6);
      setUserColors(colors);
      setUsers([...users, data]);
    })
    socket.on('user-offline', async(data) => {
      let newUsers = users.filter(item => item.id !== data.id);
      setUsers(newUsers);
      if(receiver?.id === data.id){
        setListMessages([]);
        setReceiver(undefined);
      }
    })

    socket.on('receive-message', async(data) => {
      let newUsers = users
      if(data.sender.id && !users.find(x => x.id === data.sender.id)){
        newUsers = [...users, data.sender]
        const colors = userColors;
        colors[data.sender] = '#' + Math.random().toString(16).substr(-6);
        setUserColors(colors);
        setUsers(newUsers);
      }
      if (receiver && data.sender.id === receiver.id){
        setListMessages([...listMessages, data]);
      }
      else {
        newUsers = newUsers.map(user => user.id === data.sender.id ? {...user, newMessage: (user?.newMessage | 0) + 1, listMessages: [...(user?.listMessages ? user?.listMessages : []) , data]} : user);
        setUsers(newUsers);
      }
    })
  }, [listMessages, receiver, userColors, users]);
  
  const handleUserChat = (id: string) => {
    let newUsers = users.map(user => user.id === id ? {...user, newMessage: 0} : user);
    setUsers(newUsers);
    const user = users.find(x => x.id === id);
    setReceiver(user);
    if(receiver?.id !== id){
      setListMessages(user?.listMessages || []);
    }
  }

  function onChangeListMessage(data: any): void {
    setListMessages([...listMessages, data]);
  }

  function handleChangeSeen(user: any): void {
    let newUsers = users.map(u => u.id === user.id ? {...u, newMessage: 0} : u);
    setUsers(newUsers);
  }

  return (
    <Container className="vh-100 container-fluid">
      <div className="d-flex" style={{display: 'flex', justifyContent: 'center', alignItems: 'center', height: '10%'}}>
        {/* <Button className="w-25 rounded-pill mx-1" variant="primary" onClick={() => handeClick('blue')}>Blue</Button>
        <Button className="w-25 rounded-pill mx-1" variant="warning" onClick={() => handeClick('orange')}>Orange</Button>   */}
      </div>
      <div className='d-flex flex-column flex-md-row' style={{height: 'calc(90% - 0.25rem * 4)'}}>
        <UsersBox onUserChat={handleUserChat} users={users} userColors={userColors}/>
        <ChatBox onChangeSeen={handleChangeSeen} receiver={receiver} listMessages={listMessages} receiverColor={receiver?.id ? userColors[receiver?.id] : undefined} onChangeListMessage={onChangeListMessage} />
      </div>
    </Container>
  );
}

export default ClientPage;
