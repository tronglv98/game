import React from 'react';
import { Container } from 'react-bootstrap';
import './styles.css';
import Avatar from '../Avatar';

type MessageProps = {
  data: any;
  isSender?: boolean;
  iconColor: string;
}


const Message:React.FC<MessageProps> = ({data, isSender = false, iconColor="#f2f2f2"}) => {
  return (
    <Container className={`p-1 col-sm-7 col-10 d-flex ${isSender ? 'flex-row-reverse': 'flex-row'}`} style={{margin: isSender ? '0 0 0 auto' : '0 auto 0 0'}}>
        <Avatar color={isSender ? '#474747' : iconColor} size={'25px'}>{data.sender.name.charAt(0)}</Avatar>
        <div className="d-flex align-items-center p-2" style={{color: isSender ? '#ffffff' : '#474747', backgroundColor: isSender ? '#474747' : '#f2f2f2', borderRadius: isSender ? "20px 3px 20px 20px":"3px 20px 20px 20px", whiteSpace: 'pre-wrap'}}>{data.message}</div>
    </Container>
  );
}

export default Message;
