import React from 'react';
import UserItem from '../UserItem';
import './styles.css';

type UsersBoxProps = {
    users: any[];
    userColors: any;
    onUserChat: (id: string) => void;
}

const UsersBox:React.FC<UsersBoxProps> = ({users =[], userColors={}, onUserChat}) => {

  const handleUserChat = (id: string) => {
    onUserChat(id)
  }
  return (
    <div className="col-12 col-md-3 m-1 border rounded d-flex flex-row flex-md-column overflow-auto justify-content-start height-user">  
        {users.map((user, id) => (<UserItem key={id} onClick={handleUserChat} user={user} userColor={userColors[user.id]} />))}
    </div>
  );
}

export default UsersBox;
