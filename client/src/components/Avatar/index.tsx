import React from 'react';
import './styles.css';

type UserItemProps = {
  children?: React.ReactNode;
  color?: string;
  size?: string;
}

const Avatar:React.FC<UserItemProps> = ({children, color="#f2f2f2", size="30px"}) => {
  return (
    <div className='rounded-circle d-flex justify-content-center align-items-center mx-1' style={{backgroundColor: color, height: size, aspectRatio: '1 / 1'}}>
        <p className='text-icon'>{children}</p>
    </div>
  );
}

export default Avatar;
