import React from 'react';
import './styles.css';
import Avatar from '../Avatar';

type UserItemProps = {
  user?: any;
  active?: boolean,
  userColor: string,
  onClick: (id:string) => void;
}

const UserItem:React.FC<UserItemProps> = ({user, active = false, onClick, userColor="#f2f2f2"}) => {
  return (
    <div onClick={e => onClick(user.id)} role="button" className='col-3 col-md-12 d-flex flex-column flex-md-row justify-content-center align-items-center p-2 root-user-item' style={{backgroundColor: active? '#f2f2f2':'#ffffff'}}>
        <div className='col-12 col-md-2 position-relative d-flex justify-content-center align-items-center'>
          <Avatar color={userColor} size={'40px'}>{user.name.charAt(0)}</Avatar>
          {user.newMessage >= 1 && <div className='position-absolute left rounded-circle noti'>{user?.newMessage}</div>}
        </div>
        <div className="col-12 col-md-10 text-truncate px-2">{user.name}</div>
    </div>
  );
}

export default UserItem;
