import React from 'react';
import { Container } from 'react-bootstrap';
import './styles.css';
import Avatar from '../Avatar';

type TitleMessageProps = {
  user?: any;
  userColor?: string;
}

const TitleMessage:React.FC<TitleMessageProps> = ({user, userColor="#f2f2f2"}) => {
  return (
    <Container className="justify-content-md-start align-items-center border-bottom d-flex p-2">
        <Avatar color={userColor} size={'30px'}>{user.name.charAt(0)}</Avatar>
        <div className="px-2 text-truncate text-title">{user.name}</div>
    </Container>
  );
}

export default TitleMessage;
