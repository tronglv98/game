import React, { useState, useRef, useEffect } from 'react';
import { Button, Form } from 'react-bootstrap';
import Message from '../../components/Message';
import TitleMessage from '../../components/TitleMessage';
import { socket } from '../../utils';
import ScrollToBottom from '../ScrollToBottom';
import './styles.css';

type ChatBoxProps = {
  receiver: any;
  receiverColor: string | undefined;
  listMessages: any[];
  onChangeListMessage: (user: any) => void;
  onChangeSeen: (user: any) => void
}

const ChatBox:React.FC<ChatBoxProps> = ({receiver, listMessages, receiverColor= "#f2f2f2", onChangeListMessage, onChangeSeen}) => {
  const [message, setMessage] = useState<string>("");
  const [visible, setVisible] = useState<boolean>(false);
  const scrollSection = useRef<null | HTMLDivElement>(null); 
  
  const handleSendMessage = async () => {
    socket.emit('send-message', {message, receiver:{id:receiver.id, name: receiver.name}});
    if(receiver) {
      onChangeListMessage({message, sender: {id: socket.id, name: socket.id}, receiver:{id:receiver.id, name: receiver.name}, isSender: true})
      setMessage("");
    }
  }
  function handleScroll(): void {
    if(scrollSection.current){
      scrollSection.current.scrollTo({
        top: scrollSection.current.scrollHeight,
        behavior: 'smooth',
      });
    }
  }

  useEffect(() => {
    // 👇️ scroll to bottom every time messages change
    if(scrollSection.current){
      scrollSection.current.addEventListener("scroll", () => {
          if(scrollSection.current?.scrollTop){
            if(
              scrollSection.current?.scrollHeight && scrollSection.current?.scrollTop && scrollSection.current?.offsetHeight
              &&  scrollSection.current?.scrollHeight - scrollSection.current?.clientHeight - scrollSection.current?.scrollTop > 75
            ){
              setVisible(true);
            }else{
              setVisible(false);
            }
            
          }
          else{
            setVisible(true);
          }
      });
    }
    if(!visible){
      handleScroll();
    }
  }, [listMessages, visible]);

  return (
    <div className="col-12 col-md-9 m-1 d-flex flex-column border rounded height-chat position-relative" style={{overflow:'hidden'}}>
      {receiver && <>
      <TitleMessage  user={receiver} userColor={receiverColor}/>
      <div className="overflow-scroll" style={{padding:'8px', height: '-webkit-fill-available'}} ref={scrollSection}>
        {listMessages.map((message, id) => (
            <Message data={message} key={id} isSender={message?.isSender} iconColor={receiverColor}/>
        ))}
        <ScrollToBottom onClick={handleScroll} visible={visible} />
      </div>
      <div style={{height:'60px', padding: '8px', display: 'flex'}}>
      <Form.Control
        className="rounded mx-1"
        as="textarea" 
        rows={1}
        id="message"
        value={message}
        onChange={(e) => setMessage(e.target.value)}
      />
      <Button className="rounded" style={{color: "#ffffff", backgroundColor:'#474747', border: '1px solid #474747', padding: 'revert'}} onClick={handleSendMessage}>Send</Button>
      </div>
      </>}
    </div>
  );
}

export default ChatBox;