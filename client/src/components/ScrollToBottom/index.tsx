import React from 'react';
import { BsArrowDownCircle } from "react-icons/bs";

type ChatBoxProps = {
  visible: boolean;
  onClick: () => void;
}

const ScrollToBottom:React.FC<ChatBoxProps> = ({onClick, visible}) => {
  return (
    
    <div className="position-absolute px-1 pb-1 rounded align-items-center justify-content-center" style={{left: 0, right: 0, bottom: '75px', cursor:'pointer', display: visible? 'flex': 'none'}}>
        <BsArrowDownCircle onClick={onClick} color='#474747' fontWeight={'bold'} fontSize={'25px'}/>
    </div>
  );
}

export default ScrollToBottom;