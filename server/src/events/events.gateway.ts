/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  ConnectedSocket,
  // ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  OnGatewayInit,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { ReceiveClickDto } from './dto/receive-click.dto';
import { EventsService } from './events.service';
import { MessageDto } from './dto/message.dto';

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class EventsGateway
  implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect
{
  @WebSocketServer()
  server: Server;
  runTime = 5000;

  constructor(private readonly eventsService: EventsService) {}
  handleDisconnect(client: any) {
    client.broadcast.emit('user-offline', {
      id: client.id,
      name: client.id,
    });
  }
  handleConnection(client: any, ...args: any[]) {
    client.broadcast.emit('user-online', {
      id: client.id,
      name: client.id,
    });
  }
  afterInit(server: any) {
    console.log('Init');
  }

  @SubscribeMessage('receive-click')
  async receiveClick(
    @MessageBody() receiveClickDto: ReceiveClickDto,
    @ConnectedSocket() client: Socket,
  ) {
    if (!this.eventsService.flag) {
      setTimeout(async () => {
        const data = await this.eventsService.handleChart();
        if (data) {
          client.broadcast.emit('get-data-chart', data);
        }
      }, this.runTime);
    }
    const dataClick = await this.eventsService.receiveClick(receiveClickDto);
    client.broadcast.emit('get-data-click', dataClick);
  }

  @SubscribeMessage('send-message')
  async receiveMessage(
    @MessageBody() messageDto: MessageDto,
    @ConnectedSocket() client: Socket,
  ) {
    if (messageDto?.receiver?.id) {
      client.broadcast.to(messageDto?.receiver?.id).emit('receive-message', {
        ...messageDto,
        sender: { id: client.id, name: client.id },
      });
    }
  }
}
